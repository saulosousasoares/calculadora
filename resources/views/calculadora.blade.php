@extends('layouts.default')
@section('content')
<h1>Calculadora</h1>
<form name="formCalc">
    <div class="row">
        <div class="col-sm-2">
            <label for="acao">Ação</label>
            <select class="form-control" name="acao" id="acao">
                <option value="b3sa3">B3SA3</option>
            </select>
        </div>
        <div class="col-sm-1">
            <label for="qtd">Qtd</label>
            <input value="100" type="number" class="form-control" id="qtd" name="qtd">
        </div>
        <div class="col-sm-2">
            <label for="dtini">Data Inicial</label>
            <input value="1111-11-11" type="date" class="form-control" id="dtini" name="dtini">
        </div>
        <div class="col-sm-2">
            <label for="dtfim">Data Final</label>
            <input value="2222-11-11" type="date" class="form-control" id="dtfim" name="dtfim">
        </div>
        <div class="col-sm-2">
            <button style="margin-top: 25px" type="button" class="btn btn-success" id="calcular">Calcular</button>
        </div>
    </div>
</form>
<hr>
<table name="tblprov" id="tblprov" class="table table-striped">
    <thead class="thead-dark">
        <tr class="table-active">
            <th>Dt Ex</th>
            <th>Tipo Provento</th>
            <th>Qtd de acões</th>
            <th>Dt Pag</th>
            <th>Valor</th>
            <th>Valor Bruto</th>
            <th>Valor líquido</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script>
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $('#calcular').click(function() {
        $.post(
            '/calcular',
            {
                _token: CSRF_TOKEN,
                acao: $('#acao').val(),
                dtin: $('#dtini').val(),
                dtfi: $('#dtfim').val(),
                qtd: $('#qtd').val()
            }
        ).done(function( data ) {
            console.log(data);
            $('#tblprov tbody').html('');
            $.each(data.itens, function (key, item) {
                let dtEx = item.dataEx;
                let dtPa = item.dataPagamento;
                let tpPr = item.tipoProventoNome;
                let qtdA = $('#qtd').val();
                let valP = item.valor;
                let valB = item.valorBruto;
                let valL = item.valorLiquido;

                $('#tblprov tbody').append(
                    '<tr>'+
                        '<td>'+ dtEx +'</td>'+
                        '<td>'+ tpPr +'</td>'+
                        '<td>'+ qtdA +'</td>'+
                        '<td>'+ dtPa +'</td>'+
                        '<td>'+ valP +'</td>'+
                        '<td>'+ valB +'</td>'+
                        '<td>'+ valL +'</td>'+
                    '</tr>'
                );
            });

            $('#tblprov tbody').append(
                '<tr>'+
                    '<td></td>'+
                    '<td colspan="4"><strong>TOTAL</strong></td>'+
                    '<td><strong>'+ data.total.bruto +'</strong></td>'+
                    '<td><strong>'+ data.total.liquido +'</strong></td>'+
                '</tr>'
            );
        }).fail(function() {

        });
    });
    
</script>
@endsection
