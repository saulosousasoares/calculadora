<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acao extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'descricao'
    ];

    protected $table = 'acoes';
}
