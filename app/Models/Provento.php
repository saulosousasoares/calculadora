<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provento extends Model
{
    protected $primaryKey = ['acao', 'dataEx', 'tipoProvento'];

    public $incrementing = false;

    protected $fillable = [
        'valor',
        'dataEX',
        'dataPagamento',
        'tipoProvento',
        'acao'
    ];

    protected $table = 'proventos';
}
