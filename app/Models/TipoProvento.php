<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoProvento extends Model
{
    protected $fillable = [
        'id',
        'nome',
        'sigla'
    ];

    protected $table = 'tipo_proventos';
}
