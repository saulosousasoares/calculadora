<?php

namespace App\Models\Enums;

class TipoProventoEnum extends Enum
{
    const DIVD = 1;
    const JSCP = 2;
    const BONI = 3;
    const SUBS = 4;

}
