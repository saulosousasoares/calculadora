<?php

namespace App\Models\Enums;

abstract class Enum
{
    
    private static $constCacheArray = null;
    
    private static $keywords = array(
        '__halt_compiler',
        'abstract',
        'and',
        'array',
        'as',
        'break',
        'callable',
        'case',
        'catch',
        'class',
        'clone',
        'const',
        'continue',
        'declare',
        'default',
        'die',
        'do',
        'echo',
        'else',
        'elseif',
        'empty',
        'enddeclare',
        'endfor',
        'endforeach',
        'endif',
        'endswitch',
        'endwhile',
        'eval',
        'exit',
        'extends',
        'final',
        'for',
        'foreach',
        'function',
        'global',
        'goto',
        'if',
        'implements',
        'include',
        'include_once',
        'instanceof',
        'insteadof',
        'interface',
        'isset',
        'list',
        'namespace',
        'new',
        'or',
        'print',
        'private',
        'protected',
        'public',
        'require',
        'require_once',
        'return',
        'static',
        'switch',
        'throw',
        'trait',
        'try',
        'unset',
        'use',
        'var',
        'while',
        'xor'
    );
    
    
    public static function values()
    {
        return self::getConstants();
    }
    
    public static function name($value)
    {
        return array_search($value, self::getConstants());
    }
    
    public static function valueOf($value, $strict = false)
    {
        if (!$strict) {
            $value = strtoupper($value);
        }
        
        $const = self::getConstants();
        return (isset($const[$value]) ? $const[$value]: null);
    }
    
        
    private static function getConstants()
    {
        if (self::$constCacheArray == null) {
            self::$constCacheArray = [];
        }
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new \ReflectionClass($calledClass);
            $constants = [];
            $count = 1;
            foreach ($reflect->getConstants() as $name => $value) {
                $pvWord = str_replace('__', '', $name, $count);
                if (in_array(strtolower($pvWord), self::$keywords)) {
                    $name = $pvWord;
                }
                $constants[$name] = $value;
            }
            self::$constCacheArray[$calledClass] = $constants;
        }
        return self::$constCacheArray[$calledClass];
    }
     
    public static function isValidName($name, $strict = true)
    {
        $constants = self::getConstants();
   
        if ($strict) {
            return isset($constants[$name]);
        }
   
        return isset($constants[strtoupper($name)]);
    }
    
    public static function isValidValue($value)
    {
        $values = array_values(self::getConstants());
        return in_array($value, $values, $strict = true);
    }
    
    public static function getNames()
    {
        return array_keys(self::getConstants());
    }
    
    public static function getValues()
    {
        return array_values(self::getConstants());
    }
}
