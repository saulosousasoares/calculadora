<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalcularRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'dtin.required'                    => 'Informe a data Inicial.',
            'dtin.date'                        => 'Data inicial inválida.',
            'dtfi.required'                    => 'Informe a data Final.',
            'dtfi.date'                        => 'Data Final inválida.',
            'qtd.required'                     => 'Informe a quantidade',
            'qtd.integer'                      => 'Qtd deve ser um número válido',
            'qtd.min'                          => 'Qtd mínima deve ser 1',
        ];
    }

    public function rules()
    {
        return [
            'dtin'                         => 'required|date',
            'dtfi'                         => 'required|date',
            'qtd'                          => 'required|integer|min:1',
        ];
    }
}
