<?php

/**
 * Class CalculadoraController 
 * PHP version 7.2
 * 
 * @category Controllers
 * @package  basic
 * @author   Saulo Sousa Soares <saulosousasoares@gmail.com>
 * @license  link(author, http://www.saulosousasoares.com/license)
 * @version  Beta :)
 * @link     link(author, http://www.saulosousasoares.com)
 */
namespace App\Http\Controllers;

use App\Http\Requests\CalcularRequest;
use App\Models\Acao;
use App\Models\Enums\TipoProventoEnum;
use App\Models\Provento;
use stdClass;

/**
 * Class CalculadoraController
 * 
 * @category Controllers
 * @package  basic
 * @author   Saulo Sousa Soares <saulosousasoares@gmail.com>
 * @license  link(author, http://www.saulosousasoares.com/license)
 * @link     link(author, http://www.saulosousasoares.com)
 */
class CalculadoraController extends Controller
{
    protected $layout = 'layout.default';

    public function extractDataFrom() {

        // Configuração ***************************************************
        /** 
         * @TODO A configuração poderia está em outro local como, por exemplo,
         * no banco de dados 
         */
        $config = new stdClass();
        $config->acao           = 1;
        $config->url            = 'http://bvmf.bmfbovespa.com.br/Cias-Listadas/Empresas-Listadas/ResumoProventosDinheiro.aspx?codigoCvm=21610&tab=3.1&idioma=pt-br';
        $config->dataInfoStart  = '<table class="MasterTable_SiteBmfBovespa" cellspacing="0" border="0" id="ctl00_contentPlaceHolderConteudo_grdProventos_ctl01"';
        $config->dataInfoEnd    = '</table>';
        $config->dataFormat     = 'd/m/Y';
        $config->tipoProventos  = [
            'DIVIDENDO'                 => 1,
            'JRS CAP PROPRIO'           => 2
        ];
        $config->colunms        = '';
        // Configuração fim ************************************************
        
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL, $config->url);

        $contents = curl_exec($ch);

        $start = strpos($contents, $config->dataInfoStart);
        $end = strpos($contents, $config->dataInfoEnd);

        $dataInfo = substr($contents, $start, ($end - $start));

        $DOM = new \DOMDocument;
        $DOM->loadHTML($dataInfo);

        $lines = $DOM->getElementsByTagName('tr');

        $data = [];
        $countLines = 0;
        foreach ($lines as $line) {
            $itens = $line->getElementsByTagName('td');
            foreach ($itens as $item) {
                $data[$countLines][] = $item->nodeValue;
            }
            $countLines++;
        }

        $acao = new Acao();
        $acao->id = $config->acao;
        
        $proventosList = [];
        foreach ($data as $value) {
            try{
                $provento = new Provento();
                $provento->acao = $acao->id;

                $provento->valor = (double)str_replace(',', '.', $value[2]);

                $dataEx = \DateTime::createFromFormat($config->dataFormat, $value[1]);
                $provento->dataEx = $dataEx->format('Y-m-d');

                $dataPagamento = \DateTime::createFromFormat($config->dataFormat, $value[6]);
                $provento->dataPagamento = $dataPagamento->format('Y-m-d');

                $provento->tipoProvento = $config->tipoProventos[$value[4]];

                $proventosList[] = $provento;
           
                $provento->save();
            } catch(\Illuminate\Database\QueryException $e){
                // TODO: tratar erro de existencia
                var_dump($e->getMessage()); exit;
            } catch(\Exception $e) {
                var_dump($e->getMessage()); exit;
            }
            
        }
    }

    public function index() {
        return view('calculadora');
    }

    public function calcular(CalcularRequest $request) {

        $data = $request->all();

        $from   = $data['dtin'];
        $to     = $data['dtfi'];
        $qtd    = $data['qtd'];

        $proventos = Provento::whereBetween('dataEx', [$from, $to])->get();

        return response()->json($this->calcularResponse($proventos->toArray(),$qtd));
    }

    private function calcularResponse(Array $proventos, $qtd){
        $listaProventos = [];
        $totalLiquido = 0;
        $totalBruto = 0;
        foreach ($proventos as $provento) {

            // Formando datas
            $dataEx = new \DateTime($provento['dataEx']);
            $provento['dataEx'] = $dataEx->format('d/m/Y');
            $dataPagamento = new \DateTime($provento['dataPagamento']);
            $provento['dataPagamento'] = $dataPagamento->format('d/m/Y');

            $provento['tipoProventoNome'] = TipoProventoEnum::name($provento['tipoProvento']);
            
            // Calculando valores
            $valorBruto = $qtd * $provento['valor'];
            $valorLiquido = $valorBruto;
            if ($provento['tipoProvento'] == TipoProventoEnum::JSCP) {
                $valorLiquido = ($valorBruto * 0.85);
            }

            // Calculando Totais
            $totalBruto     = $totalBruto + $valorBruto;
            $totalLiquido   = $totalLiquido + $valorLiquido;

            // Formatando Valores
            $provento['valorBruto'] = number_format(round($valorBruto, 2), 2, ',', '.');
            $provento['valorLiquido'] = number_format(round($valorLiquido, 2), 2, ',', '.');
            $provento['valor'] = number_format($provento['valor'], (strlen($provento['valor'])-2), ',', '.');

            $listaProventos[] = $provento;
        }

        return array(
            'itens' => $listaProventos,
            'total' => array(
                'liquido' => number_format(round($totalLiquido, 2), 2, ',', '.'),
                'bruto' => number_format(round($totalBruto, 2), 2, ',', '.'),
            )
        );
    }
}