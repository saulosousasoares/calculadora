<?php

use Illuminate\Database\Seeder;
use App\Models\Acao;
use App\Models\TipoProvento;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('acoes')->truncate();
        $this->createAcoes();
        DB::table('tipo_proventos')->truncate();
        $this->createTipoProventos();
    }

    private function createAcoes(){
        Acao::create(
            [
                'id'            => 1,
                'nome'          => '',
                'codigo'        => 'B3SA3'
            ]
        );
    }

    private function createTipoProventos(){
        TipoProvento::create(
            [
                'id'            => 1,
                'nome'          => 'Dividendos',
                'sigla'         => 'DIVD'
            ]
        );
        TipoProvento::create(
            [
                'id'            => 2,
                'nome'          => 'Juros sobre capital próprio',
                'sigla'         => 'JSCP'
            ]
        );
        TipoProvento::create(
            [
                'id'            => 3,
                'nome'          => 'Bonificação',
                'sigla'         => 'BONI'
            ]
        );
        TipoProvento::create(
            [
                'id'            => 4,
                'nome'          => 'Direitos de subscrição',
                'sigla'         => 'SUBS'
            ]
        );
    }
}
