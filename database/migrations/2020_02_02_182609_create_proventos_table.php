<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proventos', function (Blueprint $table) {
            $table->double('valor', 8, 2);
            $table->date('dataEx');
            $table->date('dataPagamento');
            $table->unsignedBigInteger('acao');
            $table->foreign('acao')->references('id')->on('acoes')->onDelete('cascade');
            $table->unsignedBigInteger('tipoProvento');
            $table->foreign('tipoProvento')->references('id')->on('tipo_proventos')->onDelete('cascade');
            $table->primary(['acao', 'dataEx', 'tipoProvento']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proventos');
    }
}
